import Poster from "./Poster";
import P1 from "../assets/profile1.jpg";
import P2 from "../assets/profile2.jpg";
// import P3 from "../assets/profile3.jpg";

const AllPoster = () => {
  const cv = [
    {
      id: 1,
      image: P1,
      name: "Panha",
      text: "IT",
    },
    {
      id: 2,
      image: P2,
      name: "Chivorn",
      text: "IT",
    },
    {
      id: 3,
      image: P2,
      name: "Chivorn",
      text: "IT",
    },
    {
      id: 4,
      image: P1,
      name: "Panha",
      text: "IT",
    },
  ];

  // const filterName = cv.filter((fs) => {
  //   return fs.nameE === "Panha";
  // });

  return (
    <div>
      {cv.map((product) => {
        return <Poster {...product} key={product.id} />;
      })}
    </div>
  );
};

export default AllPoster;
