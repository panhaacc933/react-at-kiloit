import { useState } from "react";

const CatchValue = () => {
  const [value, setValue] = useState("");

  function handleValue(e) {
    setValue(e.target.value);
  }

  return (
    <div>
      <h2>{value}</h2>
      <div>
        <input
          onChange={handleValue}
          value={value}
          type="text"
          placeholder="Typing..."
        />
      </div>
      <br />
      <button onClick={() => setValue("")}>Clear Value</button>
    </div>
  );
};

export default CatchValue;
