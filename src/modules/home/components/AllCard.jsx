import Card from "./Card";
import pic1 from "../../../assets/profile1.jpg";
import pic2 from "../../../assets/profile2.jpg";
import pic3 from "../../../assets/profile5.png";
import pic4 from "../../../assets/profile6.png";

const AllCard = () => {
  const employee = [
    { image: { pic1 }, name: "Sun Panha", id: "007", position: "IT" },
    { image: { pic2 }, name: "Sun Panha", id: "008", position: "IT" },
    { image: { pic3 }, name: "Sun Panha", id: "009", position: "IT" },
    { image: { pic4 }, name: "Sun Panha", id: "010", position: "IT" },
  ];

  return <div className="container">{}</div>;
};

export default AllCard;
