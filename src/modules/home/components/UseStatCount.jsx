import { useState } from "react";

const UseStatCount = () => {
  const [count, setCount] = useState(0);
  // const count = arr[0];
  // const setCount = arr[1];

  // function handleCount() {
  //   setCount(count + 1);
  // }

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>Count</button>
    </div>
  );
};

export default UseStatCount;
