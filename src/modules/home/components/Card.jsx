import "./card.css";

const Card = ({ image, name, id, position }) => {
  return (
    <div className="col-lg-3 card">
      <img src={image} alt="" />
      <h2 className="pt-4 text-danger">{name}</h2>
      <h2>{id}</h2>
      <h2 className="pb-4">{position}</h2>
    </div>
  );
};

export default Card;
