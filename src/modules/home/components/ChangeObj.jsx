import { useState } from "react";

const obj = { name: "Panha", age: "20", job: "Web Developer" };
const ChangeObj = () => {
  const [user, setUser] = useState(obj);
  const newObj = { name: "Vong", age: "55", job: "OT" };

  //   function handleChange() {
  //     setUser({ ...obj, name: "Long" });
  //   }

  return (
    <div>
      <h1>{user.name}</h1>
      <h1>{user.age}</h1>
      <h1>{user.job}</h1>
      <button onClick={() => setUser(newObj)}>Change</button>
    </div>
  );
};

export default ChangeObj;
