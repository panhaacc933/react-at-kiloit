import "./Poster.css";

const Poster = (props) => {
  const { image, name, text } = props;
  return (
    <article className="card">
      <img src={image} alt="" />
      <h2>{name}</h2>
      <p>{text}</p>
    </article>
  );
};

export default Poster;
