import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <React.Fragment>
      <div className="logo">
        <Link to={"/"}>Logo</Link>
      </div>
      <div className="Home">
        <Link to={"/"}>Home</Link>
      </div>
      <div className="Home1">
        <Link to={"Home1"}>Home1</Link>
      </div>
      <div className="Home2">
        <Link to={"Home2"}>Home2</Link>
      </div>
      <div className="Home3">
        <Link to={"Home3"}>Home3</Link>
      </div>
    </React.Fragment>
  );
};

export default Navbar;
