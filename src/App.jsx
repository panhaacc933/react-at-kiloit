import React from "react";
// import AllCard from "./modules/home/components/AllCard";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./layout/Layout";
import Home from "./modules/home/components/Home";
import Home1 from "./modules/home/components/Home1";
import Home2 from "./modules/home/components/Home2";
import Home3 from "./modules/home/components/Home3";

// import SideBar from "./layout/sidebar/SideBar";
// import AllCustomer from "./modules/home/components/AllCustomer";
// import CatchValue from "./components/CatchValue";
// import ChangeObj from "./components/ChangeObj";
// import UseStatCount from "./assets/UseStatCount";
// import Event from "./components/Event";
// import AllPoster from "./components/AllPoster";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="Home1" element={<Home1 />} />
          <Route path="Home2" element={<Home2 />} />
          <Route path="Home3" element={<Home3 />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
